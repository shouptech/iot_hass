#!/usr/bin/env python

import os
import requests

def toggle_handler(event, context):
    ha_key = os.environ['HA_KEY']
    ha_url = os.environ['HA_URL']
    ha_entity_id = os.environ['HA_ENTITY_ID']

    print('toggling %s' % ha_entity_id)

    api_url = "%s/api/services/switch/toggle" % ha_url
    headers = {'x-ha-access': ha_key}
    payload = {'entity_id': ha_entity_id}
    r = requests.post(api_url, headers=headers, json=payload)

    print(r.json())

if __name__ == "__main__":
    toggle_handler(None, None)
