# IoT Home Assistant Lambda Function

This is a simple AWS Lambda function written in Python. It's purpose is to make
requests to a Home Assistant installation, toggling a switch.

I personally use it triggered by an
[AWS IoT Button](https://aws.amazon.com/iotbutton/).

## Downloads

* v0.0.1 - [iot_hass.zip](https://gitlab.com/shouptech/iot_hass/uploads/ade9587ad89cd2085c42fd03120a1014/iot_hass.zip)

## Prerequisites

1. Something to trigger the lambda function. An AWS IoT button perhaps?
2. Home Assistant installed and configured with the switch you want to toggle.
   The Home Assistant server will need to be publicly accessible, so AWS can
   reach it. You need an API key configured.
3. An AWS account.

## Installation

1. Sign in to the AWS Management Console
2. Create a new Lambda function, use the Python 3.6 runtime environment. Create
   a new IAM role from a template. Select the "AWS IoT Button Permissions"
   template.
3. Download the ZIP file above, and upload the ZIP file to the AWS Lambda
   function.
4. Configure the handler as `iot_hass.toggle_handler`.
5. Configure the environment variables as detailed below.
6. Add a trigger. If adding an AWS IoT button as a trigger, you'll need to
   download the certificate and private key, and configure the IoT button with
   the information provided in the Lambda console.
7. Run your trigger and test!

## Environment variables

| Variable name  | Description                                                          | Example                          |
|----------------|----------------------------------------------------------------------|----------------------------------|
| `HA_ENTITY_ID` | Entity ID of the switch to toggle (from your Home Assistant install) | `switch.loft_lamp`               |
| `HA_KEY`       | API Key allowing access your Home Assistant installation             | `<yourapikey>`                   |
| `HA_URL`       | URL to access your Home Assistant installation                       | `https://hass.myawesomehome.net` |
